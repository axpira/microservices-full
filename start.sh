#!/bin/bash

docker-compose -f elk/docker-compose.yml -f mongo/docker-compose.yml -f rabbitmq/docker-compose.yml -f services/docker-compose.yml -f kong/docker-compose.yml -p microservices --project-directory . up
