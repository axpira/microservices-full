# Adiciona o plugin para gerar TID
```
curl -X POST http://localhost:8001/plugins \
    --data "name=correlation-id" \
    --data "config.header_name=MessageId" \
    --data "config.generator=uuid" \
    --data "config.echo_downstream=true"
```

# Consultar os plugins
```
curl http://localhost:8001/plugins/correlation-id
curl http://localhost:8001/plugins/syslog
```

# Criar a api
```
curl -X POST http://localhost:8001/apis/ \
    --data "name=customer-api" \
    --data "uris=/api/customers" \
    --data "upstream_url=http://customerapi:8080/customers"
```

# Remover a api
```
curl -X DELETE http://localhost:8001/apis/customer-api

```


# Faz uma consulta no customer
curl http://localhost:8000/api/customers
