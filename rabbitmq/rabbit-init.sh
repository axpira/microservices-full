#!/bin/sh
set -e

create() {
  ex_name="EX.$resource"
  qu_name="QU.$resource"

  status="$($curl_status $endpoint/exchanges/%2F/$ex_name)"
  if [ $status -eq 200 ]; then
    echo "Exchange [$ex_name] exists"
  else 
    echo "Creating exchange [$ex_name]"
    status=$($curl_status -H 'content-type:application/json' -XPUT -d'{"type":"fanout","durable":true}' "$endpoint/exchanges/%2F/$ex_name")
  
    echo "STATUS: $status"
    [ $status -ne 201 ] && echo "Error on create exchange [$ex_name]" && exit 1
  fi
  
  status="$($curl_status $endpoint/queues/%2F/$qu_name)"
  if [ $status -eq 200 ]; then
    echo "Queue [$qu_name] exists"
  else 
    echo "Creating queue [$qu_name]"
    status=$($curl_status -H 'content-type:application/json' -XPUT -d"$queue_settings" "$endpoint/queues/%2F/$qu_name")
  
    echo "STATUS: $status"
    [ $status -ne 201 ] && echo "Error on create queues [$qu_name]" && exit 1
  fi
  
  
  status="$($curl $endpoint/bindings/%2F/e/$ex_name/q/$qu_name | grep $ex_name | grep $qu_name | wc -l)"
  echo "STATUS: $status"
  if [ $status -ne 0 ]; then
    echo "Binding [$ex_name] -> [$qu_name] exists"
  else 
    echo "Creating binding [$ex_name] -> [$qu_name]"
    status=$($curl_status -H 'content-type:application/json' -XPOST -d'{"routing_key":"", "arguments":{}}' "$endpoint/bindings/%2F/e/$ex_name/q/$qu_name")
  
    echo "STATUS $status"
    [ $status -ne 201 ] && echo "Error on create binding [$ex_name] -> [$qu_name]" && exit 1
    echo "[$ex_name] -> [$qu_name] Success"
  fi
}


endpoint="http://rabbitmq1:15672/api"
curl="curl -s -u rabbitmq:rabbitmq"
curl_status="$curl -o /dev/null -w %{http_code}"

#Wait in seconds to try to connect on rabbit
TIMEOUT=30
# Number of attempts to connect
ATTEMPTS=15

echo "Waiting for startup "
RC=1
for i in `seq 1 $ATTEMPTS`; do
  echo "Attempt $i of $ATTEMPTS to connect on rabbitmq"
  $curl $endpoint/overview && RC=0 && break 
  echo -n "Waiting $TIMEOUT seconds to retry "
  for i in `seq 1 $TIMEOUT`; do
    echo -n "."
    sleep 1
  done
  echo ""
done

if [ $RC -ne 0 ]; then
  echo "ERROR Can't connect on rabbitmq"
  exit 2
fi

resource="DLX"
queue_settings='{"auto_delete":false,"durable":true,"arguments":{},"node":"rabbit@rabbitmq1"}'
create

resource="Customer"
queue_settings='{"auto_delete":false,"durable":true,"arguments":{"x-dead-letter-exchange":"EX.DLX"},"node":"rabbit@rabbitmq1"}'
create


police_ha="ha"
police_settings='{"vhost":"/","name":"ha","pattern":".*","apply-to":"queues","definition":{"ha-mode":"all"},"priority":0}'
echo "$curl_status $endpoint/policies/%2f/$police_ha"
status="$($curl_status $endpoint/policies/%2f/$police_ha)"
if [ $status -eq 200 ]; then
  echo "Police [$police_ha] exists"
else 
  echo "Creating police [$police_ha]"
  status=$($curl_status -H 'content-type:application/json' -XPUT -d"$police_settings" "$endpoint/policies/%2F/$police_ha")

  echo "STATUS: $status"
  [ $status -ne 201 ] && echo "Error on create queues [$qu_name]" && exit 1
  echo "Police OK"
fi



