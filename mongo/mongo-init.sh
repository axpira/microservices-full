#!/bin/bash

echo "INIT MONGO"
#Timeout to connect on mongo in seconds
TIMEOUT=30
replicas=(mongo1 mongo2 mongo3)
#replicas=(localhost:27017 localhost:27018 localhost:27019)

echo "Waiting for startup.."
for replica in ${replicas[@]}; do
  echo -n "Checking $replica"
  RC=1
  i=0
  while [ $RC -ne 0 ]; do
    echo -n '.'
    sleep 1
    mongo --host $replica --eval 'db' --quiet > /dev/null 2> /dev/null
    RC=$?
    i=`expr $i + 1`
    if [ $i -gt $TIMEOUT ]; then
      echo "Erro on connect [$replica]"
      exit 1
    fi
  done
  echo ""
done

members_cfg="["
priority=2
id=0
for replica in ${replicas[@]}; do
  [ $priority -eq 0 ] && members_cfg="$members_cfg ,"
  members_cfg="$members_cfg { \"_id\": $id, \"host\": \"${replica}\", \"priority\": ${priority} }"

  priority=0
  ((id++))
done
members_cfg="$members_cfg ]"
echo $members_cfg
#status=$(mongo --host rs1 --quiet --eval 'rs.status().members.length')
#if [ $? -ne 0 ]; then
  # Replicaset not yet configured
  echo SETUP.sh time now: `date +"%T" `
  mongo --host ${replicas[0]} <<EOF
     var cfg = {
          "_id": "rs",
          "version": 1,
          "members": $members_cfg
      };
      rs.initiate(cfg, { force: true });
EOF
#fi

      #rs.reconfig(cfg, { force: true });
